# DriveLog v.indev (C) 2018 William Starrs. All rights reserved
# This is not licensed by Michigan.



def setup():
    sel = ("n")
    print("Hello, new driver!")
    while sel != 'y':
        print("What's your first and last name?")
        drivername = input()
        print("Is " + drivername + " correct? Type y or n.")
        sel = input()
    sel = ("n")
    while sel != 'y':
        print("What's your license number?")
        print("This will not be shared with others.")
        licensenumber = input()
        print("Is " + licensenumber + " correct?")
        sel = input()
    print("Saving info to log.txt...")
    f = open("log.csv","w")
    f.write(str(drivername))
    f.write("\n")
    f.write(str(licensenumber))
    print("Info saved. DO NOT DELETE log.csv as it is important.")
    print("You are done with setup. Drive safe!")
    f.close()
    exit()
    

def menu():
    print("Welcome back!")
    print("Choose a setting")
    print("1. Make a log")
    print("2. View required hours")
    print("3. Export progress")
    
try:
    f = open("log.csv","r")
    menu()
except FileNotFoundError:
    setup()
